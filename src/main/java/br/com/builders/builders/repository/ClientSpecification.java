package br.com.builders.builders.repository;

import br.com.builders.builders.entity.Client;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

public class ClientSpecification {

    public static Specification<Client> getClientsBy(String name,
                                                     String cpf) {
        return (Specification<Client>) (root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (nonNull(name)) {
                Predicate socialReasonPredicate = criteriaBuilder.like(root.get("name"), "%" + name + "%");
                predicates.add(socialReasonPredicate);
            }

            if (nonNull(cpf)) {
                Predicate socialReasonPredicate = criteriaBuilder.like(root.get("cpf"), "%" + cpf + "%");
                predicates.add(socialReasonPredicate);
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
