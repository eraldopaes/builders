package br.com.builders.builders.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class SimpleClientDTO {

    @NotEmpty(message = "client.name-required")
    @Size(max = 50, message = "client.name-size")
    private String name;
}
