package br.com.builders.builders.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@DynamicUpdate
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientDTO implements Serializable {

    private Long id;

    @NotEmpty(message = "client.name-required")
    @Size(max = 50, message = "client.name-size")
    private String name;

    @CPF(message = "client.cpf-invalid")
    private String cpf;

    @NotNull(message = "client.birthdate-required")
    private LocalDate birthdate;
}
