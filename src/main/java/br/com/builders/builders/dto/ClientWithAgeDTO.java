package br.com.builders.builders.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientWithAgeDTO {

    private Long id;
    private String name;
    private String cpf;
    private LocalDate birthdate;
    private String age;
}
