package br.com.builders.builders.util;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;

@Component
public class ClientUtil {

    public String getAge(LocalDate birthdate) {
        Period period = Period.between(birthdate, LocalDate.now());
        return "Years: " + period.getYears() + " / Months: " + period.getMonths() + " / days: " + period.getDays();
    }
}
