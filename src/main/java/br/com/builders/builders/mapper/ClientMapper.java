package br.com.builders.builders.mapper;

import br.com.builders.builders.dto.ClientDTO;
import br.com.builders.builders.dto.ClientWithAgeDTO;
import br.com.builders.builders.entity.Client;
import br.com.builders.builders.util.ClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClientMapper implements EntityMapper<Client, ClientDTO> {

    private final ClientUtil clientUtil;

    @Autowired
    public ClientMapper(ClientUtil clientUtil) {
        this.clientUtil = clientUtil;
    }

    @Override
    public Client toEntity(ClientDTO clientDTO) {
        Client client = new Client();
        client.setId(clientDTO.getId());
        client.setName(clientDTO.getName());
        client.setCpf(clientDTO.getCpf());
        client.setBirthdate(clientDTO.getBirthdate());
        return client;
    }

    @Override
    public ClientDTO toDto(Client client) {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setId(client.getId());
        clientDTO.setName(client.getName());
        clientDTO.setCpf(client.getCpf());
        clientDTO.setBirthdate(client.getBirthdate());
        return clientDTO;
    }

    public Page<ClientWithAgeDTO> toDto(Page<Client> clientPage, Pageable pageable) {
        List<ClientWithAgeDTO> clientDTOS = clientPage
                .getContent()
                .stream()
                .map(client -> {
                    ClientWithAgeDTO clientWithAgeDTO = new ClientWithAgeDTO();
                    clientWithAgeDTO.setId(client.getId());
                    clientWithAgeDTO.setName(client.getName());
                    clientWithAgeDTO.setCpf(client.getCpf());
                    clientWithAgeDTO.setBirthdate(client.getBirthdate());
                    clientWithAgeDTO.setAge(clientUtil.getAge(client.getBirthdate()));
                    return clientWithAgeDTO;
                }).collect(Collectors.toList());
        return new PageImpl<>(clientDTOS, pageable, clientPage.getTotalElements());
    }
}
