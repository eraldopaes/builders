package br.com.builders.builders.service;

import br.com.builders.builders.dto.ClientDTO;
import br.com.builders.builders.dto.ClientWithAgeDTO;
import br.com.builders.builders.dto.SimpleClientDTO;
import br.com.builders.builders.entity.Client;
import br.com.builders.builders.exceptionhandler.BusinessException;
import br.com.builders.builders.mapper.ClientMapper;
import br.com.builders.builders.repository.ClientRepository;
import br.com.builders.builders.repository.ClientSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientService {

    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;

    @Autowired
    public ClientService(ClientRepository clientRepository,
                         ClientMapper clientMapper) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Page<ClientWithAgeDTO> getClientsBy(String name,
                                               String cpf,
                                               Pageable pageable) {
        Specification<Client> clientSpecification = ClientSpecification.getClientsBy(name, cpf);
        Page<Client> clientPage = clientRepository.findAll(clientSpecification, pageable);
        return clientMapper.toDto(clientPage, pageable);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ClientDTO save(ClientDTO clientDTO) {

        boolean exists = clientRepository.existsByCpf(clientDTO.getCpf());
        if (exists) {
            throw new BusinessException("client.already-exists");
        }
        Client client = clientMapper.toEntity(clientDTO);
        Client savedClient = clientRepository.save(client);
        return clientMapper.toDto(savedClient);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ClientDTO update(Long id, ClientDTO clientDTO) {

        Client dbClient = clientRepository.findById(id).orElseThrow(() -> new BusinessException("client.not-found"));

        boolean exists = clientRepository.existsByCpfAndIdNot(clientDTO.getCpf(), dbClient.getId());
        if (exists) {
            throw new BusinessException("client.updated-cpf-invalid");
        }

        dbClient.setName(clientDTO.getName());
        dbClient.setCpf(clientDTO.getCpf());
        dbClient.setBirthdate(clientDTO.getBirthdate());
        Client updatedClient = clientRepository.save(dbClient);
        return clientMapper.toDto(updatedClient);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ClientDTO patchUpdate(Long id, SimpleClientDTO simpleClientDTO) {

        Client dbClient = clientRepository.findById(id).orElseThrow(() -> new BusinessException("client.not-found"));
        dbClient.setName(simpleClientDTO.getName());
        Client updatedClient = clientRepository.save(dbClient);
        return clientMapper.toDto(updatedClient);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Long id) {
        clientRepository.findById(id).ifPresent(clientRepository::delete);
    }
}
