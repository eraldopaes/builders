package br.com.builders.builders.resource;

import br.com.builders.builders.dto.ClientDTO;
import br.com.builders.builders.dto.ClientWithAgeDTO;
import br.com.builders.builders.dto.SimpleClientDTO;
import br.com.builders.builders.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/clients")
    public ResponseEntity<Page<ClientWithAgeDTO>> getClientsBy(@RequestParam(name = "name", required = false) String name,
                                                               @RequestParam(name = "cpf", required = false) String cpf,
                                                               @RequestParam("page") int pageIndex,
                                                               @RequestParam("size") int pageSize) {
        Page<ClientWithAgeDTO> clientDTOPage = clientService.getClientsBy(name, cpf, PageRequest.of(pageIndex, pageSize));
        return new ResponseEntity(clientDTOPage, HttpStatus.OK);
    }

    @PostMapping("/clients")
    public ResponseEntity<ClientDTO> save(@Valid @RequestBody ClientDTO clientDTO) {
        ClientDTO savedClientDTO = clientService.save(clientDTO);
        return new ResponseEntity(savedClientDTO, HttpStatus.CREATED);
    }

    @PutMapping("/clients/{id}")
    public ResponseEntity<ClientDTO> update(@PathVariable Long id, @Valid @RequestBody ClientDTO clientDTO) {
        ClientDTO updatedClientDTO = clientService.update(id, clientDTO);
        return new ResponseEntity(updatedClientDTO, HttpStatus.OK);
    }

    @PatchMapping("/clients/{id}")
    public ResponseEntity<ClientDTO> patchUpdate(@PathVariable Long id, @Valid @RequestBody SimpleClientDTO simpleClientDTO) {
        ClientDTO updatedSimpleClientDTO = clientService.patchUpdate(id, simpleClientDTO);
        return new ResponseEntity(updatedSimpleClientDTO, HttpStatus.OK);
    }

    @DeleteMapping("/clients/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        clientService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
