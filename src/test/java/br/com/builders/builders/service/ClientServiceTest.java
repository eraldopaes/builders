package br.com.builders.builders.service;

import br.com.builders.builders.MockitoExtension;
import br.com.builders.builders.dto.ClientDTO;
import br.com.builders.builders.dto.SimpleClientDTO;
import br.com.builders.builders.entity.Client;
import br.com.builders.builders.exceptionhandler.BusinessException;
import br.com.builders.builders.mapper.ClientMapper;
import br.com.builders.builders.repository.ClientRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    @InjectMocks
    private ClientService clientService;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private ClientMapper clientMapper;

    @Nested
    @DisplayName("Testa método de salvar")
    public class saveTest {

        @Test
        @DisplayName("Deve salvar com sucesso")
        void should_save_with_sucess() {
            Client client = mock(Client.class);
            Client savedClient = mock(Client.class);
            ClientDTO clientDTO = mock(ClientDTO.class);

            when(clientDTO.getCpf()).thenReturn("12345");
            when(clientRepository.existsByCpf("12345")).thenReturn(false);
            when(clientMapper.toEntity(clientDTO)).thenReturn(client);
            when(clientRepository.save(client)).thenReturn(savedClient);
            when(clientMapper.toDto(savedClient)).thenReturn(clientDTO);

            clientService.save(clientDTO);

            verify(clientRepository).existsByCpf("12345");
            verify(clientMapper).toEntity(clientDTO);
            verify(clientRepository).save(client);
            verify(clientMapper).toDto(savedClient);
        }

        @Test
        @DisplayName("Deve lançar exception ao salvar cliente existente")
        void should_throw_exception() {

            ClientDTO clientDTO = mock(ClientDTO.class);

            when(clientDTO.getCpf()).thenReturn("12345");
            when(clientRepository.existsByCpf("12345")).thenReturn(true);

            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> clientService.save(clientDTO));

            verify(clientRepository).existsByCpf("12345");
            assertEquals("client.already-exists", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }
    }

    @Nested
    @DisplayName("Testa método de atualizar")
    public class updateTest {

        @Test
        @DisplayName("Deve atualizar com sucesso")
        void should_update_with_sucess() {
            Client client = mock(Client.class);
            Client updatedClient = mock(Client.class);
            ClientDTO clientDTO = mock(ClientDTO.class);

            when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
            when(clientDTO.getCpf()).thenReturn("01395110590");
            when(client.getId()).thenReturn(1L);
            when(clientRepository.existsByCpfAndIdNot("01395110590", 1L)).thenReturn(false);
            when(clientRepository.save(client)).thenReturn(updatedClient);
            when(clientMapper.toDto(updatedClient)).thenReturn(clientDTO);

            clientService.update(1L, clientDTO);

            verify(clientRepository).findById(1L);
            verify(clientRepository).existsByCpfAndIdNot("01395110590", 1L);
            verify(clientRepository).save(client);
            verify(clientMapper).toDto(updatedClient);
        }

        @Test
        @DisplayName("Deve lançar exception ao tentar atualizar um cliente inexistente")
        void should_throw_exception_because_client_not_exist() {
            ClientDTO clientDTO = mock(ClientDTO.class);
            Client client = mock(Client.class);

            when(clientRepository.findById(1L)).thenReturn(Optional.empty());
            when(clientDTO.getCpf()).thenReturn("01395110590");
            when(client.getId()).thenReturn(1L);
            when(clientRepository.existsByCpfAndIdNot("01395110590", 1L)).thenReturn(false);

            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> clientService.update(1L, clientDTO));

            verify(clientRepository).findById(1L);
            verify(clientRepository, never()).existsByCpfAndIdNot("01395110590", 1L);
            verify(clientRepository, never()).save(client);
            assertEquals("client.not-found", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }

        @Test
        @DisplayName("Deve lançar exception ao tentar atualizar um cliente com cpf de cadastrado por outro cliente")
        void should_throw_exception_because_cpf_updated_already_exist() {
            ClientDTO clientDTO = mock(ClientDTO.class);
            Client client = mock(Client.class);

            when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
            when(clientDTO.getCpf()).thenReturn("01395110590");
            when(client.getId()).thenReturn(1L);
            when(clientRepository.existsByCpfAndIdNot("01395110590", 1L)).thenReturn(true);

            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> clientService.update(1L, clientDTO));

            verify(clientRepository).findById(1L);
            verify(clientRepository).existsByCpfAndIdNot("01395110590", 1L);
            verify(clientRepository, never()).save(client);
            assertEquals("client.updated-cpf-invalid", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }
    }

    @Nested
    @DisplayName("Testa método de atualizar parcial")
    public class updatePatchTest {

        @Test
        @DisplayName("Deve atualizar com sucesso")
        void should_update_with_sucess() {
            Client client = mock(Client.class);
            Client updatedClient = mock(Client.class);
            ClientDTO clientDTO = mock(ClientDTO.class);
            SimpleClientDTO simpleClientDTO = mock(SimpleClientDTO.class);

            when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
            when(clientRepository.save(client)).thenReturn(updatedClient);
            when(clientMapper.toDto(updatedClient)).thenReturn(clientDTO);

            clientService.patchUpdate(1L, simpleClientDTO);

            verify(clientRepository).findById(1L);
            verify(clientRepository).save(client);
            verify(clientMapper).toDto(updatedClient);
        }

        @Test
        @DisplayName("Deve lançar exception ao tentar atualizar um cliente inexistente")
        void should_throw_exception() {
            SimpleClientDTO simpleClientDTO = mock(SimpleClientDTO.class);

            when(clientRepository.findById(1L)).thenReturn(Optional.empty());

            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> clientService.patchUpdate(1L, simpleClientDTO));

            verify(clientRepository).findById(1L);
            assertEquals("client.not-found", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }
    }

    @Nested
    @DisplayName("Testa método de deletar")
    public class deleteTest {

        @Test
        @DisplayName("Deve deletar o cliente com sucesso")
        void should_delete_with_sucess() {
            Client client = mock(Client.class);
            when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
            doNothing().when(clientRepository).delete(client);

            clientService.delete(1L);

            verify(clientRepository).findById(1L);
            verify(clientRepository).delete(client);
        }

        @Test
        @DisplayName("Cliente a ser deletado não existe")
        void should_not_call_delete_method_from_repository() {
            when(clientRepository.findById(1L)).thenReturn(Optional.empty());

            clientService.delete(1L);

            verify(clientRepository).findById(1L);
            verify(clientRepository, never()).delete(any(Client.class));
        }
    }
}