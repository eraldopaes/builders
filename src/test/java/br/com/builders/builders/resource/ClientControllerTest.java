package br.com.builders.builders.resource;

import br.com.builders.builders.dto.ClientDTO;
import br.com.builders.builders.dto.SimpleClientDTO;
import br.com.builders.builders.service.ClientService;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.stream.Stream;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientControllerTest {

    @InjectMocks
    private ClientController clientController;

    @Mock
    private ClientService clientService;

    @Test
    @DisplayName("Testa get")
    void getTest() {

        given()
                .standaloneSetup(clientController)
                .contentType(ContentType.JSON)
                .log().all()
                .when()
                .get("/clients?name=&cpf=&page=1&size=2")
                .then()
                .statusCode(HttpStatus.OK.value());

        verify(clientService, times(1)).getClientsBy("", "", PageRequest.of(1, 2));
    }

    @Test
    @DisplayName("Testa post")
    public void postTest() {

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setName("Eraldo");
        clientDTO.setCpf("01395110590");
        clientDTO.setBirthdate(LocalDate.of(1984, 3, 24));

        given()
                .standaloneSetup(clientController)
                .contentType(ContentType.JSON)
                .body(clientDTO)
                .log().all()
                .when()
                .post("/clients")
                .then()
                .statusCode(HttpStatus.CREATED.value());

        verify(clientService).save(clientDTO);
    }

    @Test
    @DisplayName("Testa put")
    public void putTest() {

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setId(1L);
        clientDTO.setName("Eraldo");
        clientDTO.setCpf("01395110590");
        clientDTO.setBirthdate(LocalDate.of(1984, 3, 24));

        given()
                .standaloneSetup(clientController)
                .contentType(ContentType.JSON)
                .body(clientDTO)
                .log().all()
                .when()
                .put("/clients/{id}", 1L)
                .then()
                .statusCode(HttpStatus.OK.value());

        verify(clientService).update(1L, clientDTO);
    }

    @Test
    @DisplayName("Testa patch")
    public void patchTest() {

        SimpleClientDTO simpleClientDTO = new SimpleClientDTO();
        simpleClientDTO.setName("Eraldo");

        given()
                .standaloneSetup(clientController)
                .contentType(ContentType.JSON)
                .body(simpleClientDTO)
                .log().all()
                .when()
                .patch("/clients/{id}", 1L)
                .then()
                .statusCode(HttpStatus.OK.value());

        verify(clientService).patchUpdate(1L, simpleClientDTO);
    }

    @Test
    @DisplayName("Testa delete")
    public void deleteTest() {

        given()
                .standaloneSetup(clientController)
                .log().all()
                .when()
                .delete("/clients/{id}", 1L)
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());

        verify(clientService).delete(1L);
    }

    @ParameterizedTest
    @MethodSource("provideClientsWithError")
    @DisplayName("Deve lançar exception ao tentar salvar um cliente com erro de validacao")
    public void save_error(String name, String cpf, LocalDate birthdate) {

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setName(name);
        clientDTO.setCpf(cpf);
        clientDTO.setBirthdate(birthdate);

        given()
                .standaloneSetup(clientController)
                .contentType(ContentType.JSON)
                .body(clientDTO)
                .log().all()
                .when()
                .post("/clients")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        verify(clientService, never()).save(clientDTO);
    }

    private static Stream<Arguments> provideClientsWithError() {
        return Stream.of(
                Arguments.of("Eraldo", "00", LocalDate.now()),         // CPF inválido
                Arguments.of("Eraldo", "01395110590", null),           // Nao informou nascimento
                Arguments.of("", "01395110590", LocalDate.now()),      // Nao informou nome
                Arguments.of(null, "01395110590", LocalDate.now())     // Nome invalido
        );
    }
}