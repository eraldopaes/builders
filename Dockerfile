FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY ./target/builders-0.0.1-SNAPSHOT.jar builders.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","/builders.jar"]